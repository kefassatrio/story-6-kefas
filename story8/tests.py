from django.test import TestCase, Client

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import reverse, resolve

from .views import view_story8


class TestStory7(TestCase):
    def setUp(self):
        self.client = Client()
        options = Options()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        # self.browser = webdriver.Chrome(chrome_options=options, executable_path="Status/chrome_77_driver/chromedriver.exe")
        self.browser = webdriver.Chrome(chrome_options=options)

    def tearDown(self):
        self.browser.close()

    #functional test
    def test_title(self):
        self.browser.get('http://localhost:8000/story8')
        # self.browser.get('http://statuskefas.herokuapp.com/story8')

        self.assertEquals(self.browser.title, 'Story 8 Kefas')
        # self.browser.close()
    def test_ada_table(self):
        # self.browser = webdriver.Chrome('Status/chrome_77_driver/chromedriver.exe')
        self.browser.get('http://localhost:8000/story8')
        # self.browser.get('http://statuskefas.herokuapp.com/story8')

        self.assertIn("<table", self.browser.page_source)
    
    def test_ada_input_text(self):
        # self.browser = webdriver.Chrome('Status/chrome_77_driver/chromedriver.exe')
        self.browser.get('http://localhost:8000/story8')
        # self.browser.get('http://statuskefas.herokuapp.com/story8')
        self.assertIn('<input type="text"', self.browser.page_source)
    
    def test_ada_input_submit(self):
        # self.browser = webdriver.Chrome('Status/chrome_77_driver/chromedriver.exe')
        self.browser.get('http://localhost:8000/story8')
        # self.browser.get('http://statuskefas.herokuapp.com/story8')
        self.assertIn('<input type="submit"', self.browser.page_source)
    
    #unittest
    def test_pake_html_bener(self):
        response = self.client.get('/story8/')
        self.assertTemplateUsed(response, 'story8.html')
