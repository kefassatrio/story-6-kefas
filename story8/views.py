from django.shortcuts import render
import requests
from django.http import JsonResponse

# Create your views here.
def view_story8(request):
    return render(request, 'story8.html')

# def get_data(request):
#     query = request.GET["query"]
#     url = "https://www.googleapis.com/books/v1/volumes?q=" + query 
#     response = requests.get(url)
#     response_json = response.json()

#     return JsonResponse(response_json)