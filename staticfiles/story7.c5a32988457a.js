$(document).ready(()=>{
    // $("#accordion").accordion({header: "h2", collapsible: true, active: false});
    let isDark = false;

    $(".title").siblings().hide()
    
    $(".title").on('click', function() {
        // console.log($(this).html())
        $(".title").removeClass("title-shown-light")
        $(".title").removeClass("title-shown-dark")
        console.log("masuk sini")
        if(!$(this).siblings().hasClass("shown")){
            $(".title").siblings().slideUp()
            $(".title").siblings().removeClass("shown")
            $(this).siblings().slideDown()
            $(this).siblings().addClass("shown")
            if(isDark){
                $(this).addClass("title-shown-dark")
            }
            else{
                $(this).addClass("title-shown-light")
            }
        }
        else{
            $(".title").siblings().removeClass("shown")
            $(".title").removeClass("title-shown-light")
            $(".title").removeClass("title-shown-dark")
            $(".title").siblings().slideUp()            
        }
    })
    $(".toggle").on("click", function(){
        if(isDark){
            $(".toggle").removeClass("isDark")
            $("body").css({backgroundColor:"white", backgroundImage:"url(/static/img/bottom.png)"})
            $("h1").css({color:"var(--darkpurple)"})
            $("h2").css({backgroundColor:"var(--darkpurple)"})
            if($(".title").hasClass("title-shown-dark")){
                $(".title").removeClass("title-shown-dark")
            }
            isDark = !isDark;
        }
        else{
            $(".toggle").addClass("isDark")
            $("body").css({backgroundColor:"var(--darkpurple)", backgroundImage:"url(/static/img/bottom-dark.png)"})
            $("h1").css({color:"white"})
            $("h2").css({backgroundColor:"white"})
            if($(".title").hasClass("title-shown-light")){
                $(".title").removeClass("title-shown-light")
            }
            isDark = !isDark;
        }

    })
})