const formatBook = (books) => {
    $('#results').append(    
        "<tr>" +
            "<th>Title</th>" +
            "<th>Book Cover</th>" +
            "<th>Author(s)</th>" +
            "<th>Details</th>" +
        "</tr>"
    );
    for(i=0 ; i<books.items.length; i++){
        $('#results').append("<tr>");
        $('#results').append("<td><p>" + books.items[i].volumeInfo.title + "</p></td>");                    
        try{    
        $('#results').append("<td><img src='" + books.items[i].volumeInfo.imageLinks.thumbnail + "'></img></td>");
        }
        catch{
            $("#results").append("<td><p> No image </p></td>");                   
        }
        try{
            if(books.items[i].volumeInfo.authors.length > 0){
                $("#results").append(`
                <td>
                    <ul>
                        ${books.items[i].volumeInfo.authors.map((obj) => {
                            return `<li>${obj}</li>`
                        }).join("")}
                    </ul>
                </td>
                `)
            }
            else{
                $("#results").append("<td><p> No known authors </p></td>");
            }
        }
        catch{
            $("#results").append("<td><p> No known authors </p></td>");  
        }
        try{
            $('#results').append("<td><a href='" + books.items[i].volumeInfo.infoLink + "'>Details</a></td>");  
        }
        catch{
            $("#results").append("<td><p> No details </p></td>");  
        }
        $('#results').append("</tr>");
    }
}
$(document).ready(() => {
    $('#results')[0].innerHTML = "<h3> Loading page... </h3>";
    

    $.ajax({
        method: 'GET',
        url: 'https://www.googleapis.com/books/v1/volumes?q=megalodon',
        success: function(books){
            console.log(books);
            $('#results').empty();
            formatBook(books)
        }
    })

    $('#button').on('click', function(){
        let key = $("#search").val();
        $('#results')[0].innerHTML = "<h3> Searching... </h3>"

        $.ajax({
            method: 'GET',
            url: 'https://www.googleapis.com/books/v1/volumes?q=' + key,
            success: function(books){
                // console.log(books.items[0].searchInfo);
                console.log(books.items)
                $('#results').empty();
                formatBook(books)

            }
        })
    })

})