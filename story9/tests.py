from django.test import TestCase, Client

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import reverse, resolve

from .views import view_story9, view_login, view_logout


class TestStory7(TestCase):
    def setUp(self):
        self.client = Client()
        options = Options()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        # self.browser = webdriver.Chrome(chrome_options=options, executable_path="Status/chrome_77_driver/chromedriver.exe")
        self.browser = webdriver.Chrome(chrome_options=options)

    def tearDown(self):
        self.browser.close()

    #functional test
    def test_title(self):
        self.browser.get('http://localhost:8000/story9')
        # self.browser.get('http://statuskefas.herokuapp.com/story8')

        self.assertEquals(self.browser.title, 'Story 9 Kefas')
        # self.browser.close()

    def test_ada_login(self):
        # self.browser = webdriver.Chrome('Status/chrome_77_driver/chromedriver.exe')
        self.browser.get('http://localhost:8000/story9/login')
        # self.browser.get('http://statuskefas.herokuapp.com/story8')
        self.assertIn('<input type="submit" id="login" value="Login"', self.browser.page_source)

    def test_suruh_login(self):
        self.browser.get('http://localhost:8000/story9')
        self.assertIn('Please login!', self.browser.page_source)
        self.assertIn('<button id="login">Login</button>', self.browser.page_source)
    
    def test_login_logout(self):
        self.browser.get('http://localhost:8000/story9/login')
        user = self.browser.find_element_by_id("id_username")
        password = self.browser.find_element_by_id("id_password")
        login = self.browser.find_element_by_id("login")
        user.send_keys("kefassatrio")
        password.send_keys("kefas1234")
        login.click()
        self.assertIn("<h1>Hello, kefassatrio</h1>", self.browser.page_source)
        logout = self.browser.find_element_by_id("logout")
        logout.click()
        self.assertIn("Please login!", self.browser.page_source)

    #unittest
    def test_pake_html_bener(self):
        response = self.client.get('/story9/')
        self.assertTemplateUsed(response, 'home.html')
    
    def test_pake_view_story9(self):
        handler = resolve('/story9/')
        self.assertEqual(handler.func, view_story9)

    def test_pake_view_login(self):
        handler = resolve('/story9/login/')
        self.assertEqual(handler.func, view_login)
        
    def test_pake_view_logout(self):
        handler = resolve('/story9/logout/')
        self.assertEqual(handler.func, view_logout)