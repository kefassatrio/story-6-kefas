from django.test import TestCase, Client

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import reverse, resolve

from .views import view_story7


class TestStory7(TestCase):
    def setUp(self):
        self.client = Client()
        options = Options()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        # self.browser = webdriver.Chrome(chrome_options=options, executable_path="Status/chrome_77_driver/chromedriver.exe")
        self.browser = webdriver.Chrome(chrome_options=options)

    def tearDown(self):
        self.browser.close()

    #functional test

    def test_title(self):
        # self.browser = webdriver.Chrome('Status/chrome_77_driver/chromedriver.exe')
        # self.browser.get('http://localhost:8000/story7')
        self.browser.get('http://statuskefas.herokuapp.com/story7')

        self.assertEquals(self.browser.title, 'Story 7 Kefas')
        # self.browser.close()

    def test_bisa_accordion_activities(self):
        # self.browser = webdriver.Chrome('Status/chrome_77_driver/chromedriver.exe')
        # self.browser.get('http://localhost:8000/story7')
        self.browser.get('http://statuskefas.herokuapp.com/story7')

        activities = self.browser.find_element_by_id("activities")
        activities.click()
        self.assertIn( "- Staff of IT Development Department", self.browser.page_source)

    def test_bisa_accordion_organizations(self):
        # self.browser = webdriver.Chrome('Status/chrome_77_driver/chromedriver.exe')
        # self.browser.get('http://localhost:8000/story7')
        self.browser.get('http://statuskefas.herokuapp.com/story7')

        organizations = self.browser.find_element_by_id("organizations")
        organizations.click()
        self.assertIn("- BEM PTI Fasilkom UI", self.browser.page_source)
    
    def test_bisa_accordion_achievements(self):
        # self.browser = webdriver.Chrome('Status/chrome_77_driver/chromedriver.exe')
        # self.browser.get('http://localhost:8000/story7')
        self.browser.get('http://statuskefas.herokuapp.com/story7')

        achievements = self.browser.find_element_by_id("achievements")
        achievements.click()
        self.assertIn("- Cooked a 3-minute instant cup noodles in under 2 minutes", self.browser.page_source)


    #unittest

    def test_pake_view_status(self):
        handler = resolve('/story7')
        self.assertEqual(handler.func, view_story7)
    def test_pake_html_bener(self):
        response = self.client.get('/story7')
        self.assertTemplateUsed(response, 'story7.html')

    # def test_waktu_auto_add_now(self):
    #     status = Status.objects.create(status="halooo")
    #     status.time = datetime.datetime.now()
    #     status.save()

    #     print(status.time.strftime("%d %b %Y %H:%M"))
    #     self.assertEquals(status.time.strftime("%d %b %Y %H:%M"), datetime.datetime.now().strftime("%d %b %Y %H:%M"))