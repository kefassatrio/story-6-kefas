from django.urls import path, include
from .views import view_story7

app_name="Story7"
urlpatterns = [
    path('', view_story7, name="story7"),
]