# Generated by Django 2.2.6 on 2019-11-01 16:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Status', '0003_auto_20191101_2255'),
    ]

    operations = [
        migrations.AlterField(
            model_name='status',
            name='time',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
