from django.urls import path, include
from .views import view_status

app_name="Status"
urlpatterns = [
    path('', view_status, name="status"),
]
