from django.test import TestCase, Client

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import reverse, resolve
import time, datetime, pytz

from .models import Status
from .views import view_status


class TestStatus(TestCase):
    def setUp(self):
        self.client = Client()
        options = Options()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        # self.browser = webdriver.Chrome(chrome_options=options, executable_path="Status/chrome_77_driver/chromedriver.exe")
        self.browser = webdriver.Chrome(chrome_options=options)

    def tearDown(self):
        self.browser.close()

    #functional test
    def test_ada_halo(self):
        # self.browser = webdriver.Chrome('Status/chrome_77_driver/chromedriver.exe')
        # self.browser.get('http://localhost:8000')
        self.browser.get('http://statuskefas.herokuapp.com')

        halo = self.browser.find_element_by_class_name('halo').text
        self.assertEquals(halo, "Halo, apa kabar?")
        # self.browser.close()

    def test_title(self):
        # self.browser = webdriver.Chrome('Status/chrome_77_driver/chromedriver.exe')
        # self.browser.get('http://localhost:8000')
        self.browser.get('http://statuskefas.herokuapp.com')

        self.assertEquals(self.browser.title, 'Status Kefas')
        # self.browser.close()

    def test_bisa_bikin_status(self):
        # self.browser = webdriver.Chrome('Status/chrome_77_driver/chromedriver.exe')
        # self.browser.get('http://localhost:8000')
        self.browser.get('http://statuskefas.herokuapp.com')

        status_form = self.browser.find_element_by_id("input-status")
        submit = self.browser.find_element_by_id("submit")
        status_form.send_keys("Coba Coba")
        submit.click()
        self.assertIn( "Coba Coba", self.browser.page_source)
        # self.browser.close()

    def test_bener_ga_tanggal_nya(self):
        # self.browser = webdriver.Chrome('Status/chrome_77_driver/chromedriver.exe')
        # self.browser.get('http://localhost:8000')
        self.browser.get('http://statuskefas.herokuapp.com')

        status_form = self.browser.find_element_by_id("input-status")
        submit = self.browser.find_element_by_id("submit")
        status_form.send_keys("Testing waktu")
        submit.click()

        self.assertIn(datetime.datetime.now().strftime("%d %b %Y | %H:%M"), self.browser.page_source)

    #unittest
    def test_model_str(self):
        obj = Status.objects.create(status="halo")
        self.assertEqual(str(obj), "halo")

    def test_pake_view_status(self):
        handler = resolve('/')
        self.assertEqual(handler.func, view_status)

    def test_habis_post_redirect_ke_halaman_yang_sama(self):
        response = self.client.post('/', {
            'status': "mau makan"
        })
        self.assertEquals(response.status_code, 302)

    def test_ada_halaman_status(self):
        response = self.client.get('/')
        self.assertEquals(response.status_code, 200)

    def test_kalo_ga_ada_halaman(self):
        response = self.client.get('/halaman-ini-gaada-woi')
        self.assertEquals(response.status_code, 404)

    # def test_waktu_auto_add_now(self):
    #     status = Status.objects.create(status="halooo")
    #     status.time = datetime.datetime.now()
    #     status.save()

    #     print(status.time.strftime("%d %b %Y %H:%M"))
    #     self.assertEquals(status.time.strftime("%d %b %Y %H:%M"), datetime.datetime.now().strftime("%d %b %Y %H:%M"))