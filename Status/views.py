from django.shortcuts import render, redirect
from .forms import StatusForm
from .models import Status
import datetime
# Create your views here.
def view_status(request):
    if request.method == "POST":
        form = StatusForm(request.POST)
        if form.is_valid():
            status = form.save(commit=False)
            status.time = datetime.datetime.now()
            status.save()
            return redirect('Status:status')
    else:
        form = StatusForm()
    status_objects = Status.objects.all().order_by("time").reverse()
    return render(request, 'index.html', {"form": form, "status_objects": status_objects})